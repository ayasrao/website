<?php
//Start session
session_start();
 
//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['sess_user_id']) || (trim($_SESSION['sess_user_id']) == '')) {
	header("location: login.html");
	exit();
}
?>
<!DOCTYPE html>
<html>
<head>
<title> Main page</title>
<meta name="viewport" content="width=device width, initial-scale=1.0">
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<link href="bootstrap.min.css" rel="stylesheet" media="screen">
<link href="file:///C|/xampp/htdocs/bootstrap-master/dist/css/bootstrap.css" rel="stylesheet" media="screen">
<script src="assets/js/ie-emulation-modes-warning.js"></script>
</head>
<body>
<nav class="navbar navbar-default" role="navigation">
   <div class="navbar-header">
      <a class="navbar-brand" href="#">My Website</a>
   </div>
   <div>
      <ul class="nav navbar-nav">
         <li class="active"><a href="http://localhost/firstweb/home.php">Home</a></li>
         <li><a href="http://localhost/firstweb/list.php">Entries</a></li>
         
            
               <li><a href="#">Contact Us</a></li>
               <li><a href="#">FAQs</a></li>
               <li><a href="form.php">SignUp</a></li>
               <li><a href="we.php">Login</a></li>
               <li class="divider"></li>
               <li><a href="https://www.facebook.com">Facebook</a></li>
               <li class="divider"></li>
               <li><a href="http://www.google.com.pk">Google</a></li>
            
         
      </ul>
   </div>
</nav>
<h1>Welcome, <?php echo $_SESSION["sess_username"] ?></h1>
<footer>
<div class="container">
<hr>
        <div class="text-center center-block">
            <p class="txt-railway">- contact us -</p>
            <br />
                <a href="https://www.facebook.com/ayaz.rao.9"><i id="social" class="fa fa-facebook-square fa-3x social-fb"></i></a>
	            <a href="https://twitter.com/bootsnipp"><i id="social" class="fa fa-twitter-square fa-3x social-tw"></i></a>
	            <a href="https://plus.google.com/+Bootsnipp-page"><i id="social" class="fa fa-google-plus-square fa-3x social-gp"></i></a>
	            <a href="mailto:bootsnipp@gmail.com"><i id="social" class="fa fa-envelope-square fa-3x social-em"></i></a>
</div>
    </hr>
</div>

</footer>

<!-- Social Footer, Single Coloured -->
<!-- Include Font Awesome Stylesheet in Header -->

<!-- // -->



</body>
</html>
